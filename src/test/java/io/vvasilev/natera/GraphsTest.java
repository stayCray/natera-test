package io.vvasilev.natera;

import org.junit.Assert;
import org.junit.Test;

/**
 * Tests for {@link Graphs}
 */
public class GraphsTest {

    /**
     * Test {@link Graphs#newDirected()} on returning directed graph
     *
     */
    @Test
    public void test_newDirected() {
        Graph<String, ? extends Edge<String>> g = Graphs.newDirected();
        Assert.assertNotNull(g);
        Assert.assertTrue(g instanceof DirectedGraph);
    }

    /**
     * Test {@link Graphs#newUndirected()} on returning undirected graph
     *
     */
    @Test
    public void test_newUndirected() {
        Graph<String, ? extends Edge<String>> g = Graphs.newUndirected();
        Assert.assertNotNull(g);
        Assert.assertTrue(g instanceof UndirectedGraph);
    }
}
