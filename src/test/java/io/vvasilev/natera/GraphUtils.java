package io.vvasilev.natera;

import java.util.*;
import java.util.function.BiConsumer;

/**
 * Utility methods for {@link Graph}.
 *
 */
// TODO: 9/14/2020 Consider making members of Graph or Graphs classes
public class GraphUtils {

    /**
     * Utility method to add multiple vertices at once to a graph.
     *
     *
     * @param graph
     * @param vertices
     * @param <T>
     */
    public static <T> void addVertices(Graph<T, ? extends Edge<T>> graph, T... vertices) {
        Arrays.asList(vertices).forEach(graph::addVertex);
    }

    /**
     * Utility method to covert list of edges to list of vertices.
     * For example, for path [0-2, 2-3, 3-1] resulting list expected to be [0, 2, 3, 1]
     *
     * @param path
     * @param <T>
     * @return
     */
    public static <T> List<T> toVertexList(List<Edge<T>> path) {
        Objects.requireNonNull(path);
        LinkedHashSet<T> vertices = new LinkedHashSet<>();
        path.forEach(edge -> vertices.addAll(edge.asList()));
        return new ArrayList<>(vertices);
    }

    public static <T> void forEachPair(List<Edge<T>> path, BiConsumer<Edge<T>, Edge<T>> consumer) {
        for (int i=0; i< path.size() - 1; i++) {
            consumer.accept(path.get(i), path.get(i + 1));
        }
    }

    public static <T> boolean testIfFormPath(Edge<T> edge1, Edge<T> edge2) {
        return edge1.getHead().equals(edge2.getTail());
    }
}
