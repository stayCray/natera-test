package io.vvasilev.natera;

import org.junit.Assert;
import org.junit.Test;

import java.util.Arrays;
import java.util.List;

/**
 * Tests for {@link DirectedGraph}
 */
public class DirectedGraphTest {

    /** Tests for {@link Graph#addVertex(Object)} **/

    @Test
            (expected = NullPointerException.class)
    public void test_addVertex_to_fail_on_null() {
        DirectedGraph<String> directedGraph = new DirectedGraph<>();
        directedGraph.addVertex(null);
    }

    @Test
    public void test_addVertex() {
        DirectedGraph<String> directedGraph = new DirectedGraph<>();
        directedGraph.addVertex("1");
    }

    @Test
            (expected = IllegalArgumentException.class )
    public void test_addVertex_to_fail_for_duplicated_vertex() {
        DirectedGraph<String> directedGraph = new DirectedGraph<>();
        directedGraph.addVertex("1");
        directedGraph.addVertex("1");
    }

    /*** Tests for {@link Graph#addEdge(Object, Object)} **/

    @Test
            (expected = NullPointerException.class)
    public void test_addEdge_to_fail_if_vertex_1_null() {
        DirectedGraph<String> directedGraph = new DirectedGraph<>();
        directedGraph.addEdge(null, "2");
    }

    @Test
            (expected = NullPointerException.class)
    public void test_addEdge_to_fail_if_vertex_2_null() {
        DirectedGraph<String> directedGraph = new DirectedGraph<>();
        directedGraph.addEdge("1", null);
    }

    @Test
            (expected = IllegalStateException.class)
    public void test_addEdge_to_fail_if_vertex_1_not_added_first() {
        DirectedGraph<String> directedGraph = new DirectedGraph<>();
        directedGraph.addVertex("2");
        directedGraph.addEdge("1", "2");
    }

    @Test
            (expected = IllegalStateException.class)
    public void test_addEdge_to_fail_if_vertex_2_not_added_first() {
        DirectedGraph<String> directedGraph = new DirectedGraph<>();
        directedGraph.addVertex("1");
        directedGraph.addEdge("1", "2");
    }

    @Test
    public void test_addEdge() {
        DirectedGraph<String> directedGraph = new DirectedGraph<>();
        directedGraph.addVertex("1");
        directedGraph.addVertex("2");
        directedGraph.addEdge("1", "2");
    }

    @Test
            (expected = IllegalArgumentException.class)
    public void test_addEdge_to_fail_for_duplicated_edge() {
        DirectedGraph<String> directedGraph = new DirectedGraph<>();
        directedGraph.addVertex("1");
        directedGraph.addVertex("2");
        directedGraph.addEdge("1", "2");
        directedGraph.addEdge("1", "2");
    }

    @Test
            (expected = IllegalArgumentException.class)
    public void test_addEdge_to_fail_for_loop() {
        DirectedGraph<String> directedGraph = new DirectedGraph<>();
        directedGraph.addVertex("1");
        directedGraph.addEdge("1", "1");
    }

    /*** Tests for {@link Graph#getPath(Object, Object)} **/

    @Test
            (expected = NullPointerException.class)
    public void test_getPath_to_fail_if_vertex_1_null() {
        DirectedGraph<String> directedGraph = new DirectedGraph<>();
        directedGraph.getPath(null, "2");
    }

    @Test
            (expected = NullPointerException.class)
    public void test_getPath_to_fail_if_vertex_2_null() {
        DirectedGraph<String> directedGraph = new DirectedGraph<>();
        directedGraph.getPath("1", null);
    }

    @Test
            (expected = IllegalStateException.class)
    public void test_getPath_to_fail_if_vertex_1_not_added_first() {
        DirectedGraph<String> directedGraph = new DirectedGraph<>();
        directedGraph.addVertex("2");
        directedGraph.addEdge("1", "2");
    }

    @Test
            (expected = IllegalStateException.class)
    public void test_getPath_to_fail_if_vertex_2_not_added_first() {
        DirectedGraph<String> directedGraph = new DirectedGraph<>();
        directedGraph.addVertex("1");
        directedGraph.addEdge("1", "2");
    }

    @Test
            (expected = IllegalArgumentException.class)
    public void test_getPath_to_fail_for_loop() {
        DirectedGraph<String> directedGraph = new DirectedGraph<>();
        directedGraph.addVertex("1");
        directedGraph.getPath("1", "1");
    }

    @Test
    public void test_getPath_sample_0() {
        DirectedGraph<String> g = new DirectedGraph<>();

        GraphUtils.addVertices(g, "0", "1", "2", "3");

        List<Edge<String>> path = g.getPath("0", "3");
        Assert.assertNull(path);
    }

    @Test
    public void test_getPath_sample_1() {
        UndirectedGraph<String> g = new UndirectedGraph<>();

        GraphUtils.addVertices(g, "0", "1");
        g.addEdge("0", "1");

        List<Edge<String>> path = g.getPath("0", "1");
        Assert.assertEquals(Arrays.asList("0", "1"), GraphUtils.toVertexList(path));
    }

    @Test
    public void test_getPath_sample_2() {
        DirectedGraph<String> g = new DirectedGraph<>();

        GraphUtils.addVertices(g, "0", "1", "2", "3");

        g.addEdge("0", "1");
        g.addEdge("1", "0");
        g.addEdge("1", "2");
        g.addEdge("2", "3");

        List<Edge<String>> path = g.getPath("0", "3");
        GraphUtils.forEachPair(path, (left, right) -> Assert.assertTrue(GraphUtils.testIfFormPath(left, right)));
        Assert.assertEquals(Arrays.asList("0", "1", "2", "3"), GraphUtils.toVertexList(path));
    }

    @Test
    public void test_getPath_sample_3() {
        DirectedGraph<String> g = new DirectedGraph<>();

        GraphUtils.addVertices(g, "0", "1", "2", "3");
        g.addEdge("1", "2");

        List<Edge<String>> path = g.getPath("2", "1");
        Assert.assertNull(path);
    }

    @Test
    public void test_getPath_sample_4() {
        DirectedGraph<String> g = new DirectedGraph<>();

        GraphUtils.addVertices(g, "0", "1", "2", "3");
        g.addEdge("0", "1");
        g.addEdge("1", "2");
        g.addEdge("2", "0");

        List<Edge<String>> path = g.getPath("1", "3");
        Assert.assertNull(path);
    }

}
