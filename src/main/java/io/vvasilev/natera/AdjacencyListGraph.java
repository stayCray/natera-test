package io.vvasilev.natera;

import java.util.*;

/**
 * Implementation of {@link Graph} stored as adjacency list.
 * Uses a hash table to associate each vertex in a graph with a set of adjacent vertices.
 * There is no explicit representation of edges as objects.
 *
 * <p>Loops are not allowed. Throws {@link IllegalArgumentException} on attempt to add one.
 * <p>Multiple edges joining the same pair multiple times are not allowed. Throws {@link IllegalArgumentException}
 *
 * @param <T> type of vertices
 *
 * @see UndirectedGraph
 */
class AdjacencyListGraph<T> implements Graph<T, Edge<T>>{
    private final Map<T, Set<T>> vertices = new HashMap<>();

    private final boolean undirected;

    AdjacencyListGraph(boolean undirected) {
        this.undirected = undirected;
    }

    /**
     * @see Graph#addVertex(Object)
     *
     * @param vertex vertex to add.
     */
    @Override
    public final void addVertex(T vertex) {
        Objects.requireNonNull(vertex);
        checkForVertexDuplicate(vertex);

        vertices.put(vertex, new HashSet<>());
    }

    /**
     * @see Graph#addEdge(Object, Object)
     *
     * @param vertex1 endpoint of the edge being added
     * @param vertex2 endpoint of the edge being added
     *
     * @throws NullPointerException if any of vertices is null
     * @throws IllegalStateException if any of vertices is not added by means of {@link #addVertex(Object)} first
     * @throws IllegalArgumentException if vertices are equal
     */
    @Override
    public final void addEdge(T vertex1, T vertex2) {
        Objects.requireNonNull(vertex1);
        Objects.requireNonNull(vertex2);
        checkForVertexToBeInGraph(vertex1);
        checkForVertexToBeInGraph(vertex2);
        checkForLoop(vertex1, vertex2);
        checkForEdgeDuplicate(vertex1, vertex2);

        vertices.get(vertex1).add(vertex2);
        if (undirected) {
            vertices.get(vertex2).add(vertex1);
        }
    }

    /**
     * @see Graph#getPath(Object, Object)
     *
     * @param vertex1 vertex specifying the start of the path
     * @param vertex2 vertex specifying the end of the path
     * @return List of edges, null if there no path found
     *
     * @throws NullPointerException if any of vertices is null
     * @throws IllegalStateException if any of vertices is not added by means of {@link #addVertex(Object)} first
     * @throws IllegalArgumentException if vertices are equal
     */
    @Override
    public final List<Edge<T>> getPath(T vertex1, T vertex2) {
        Objects.requireNonNull(vertex1);
        Objects.requireNonNull(vertex2);
        checkForVertexToBeInGraph(vertex1);
        checkForVertexToBeInGraph(vertex2);
        checkForLoop(vertex1, vertex2);

        List<T> path = findAnyPath(vertex1, vertex2, new HashSet<>());

        return toList(path);
    }

    private void checkForVertexDuplicate(T vertex) {
        if (vertices.containsKey(vertex)) {
            throw new IllegalArgumentException("Duplicated vertex: " + vertex);
        }
    }

    private void checkForVertexToBeInGraph(T vertex) {
        if (!vertices.containsKey(vertex)) {
            throw new IllegalStateException("Vertex is not in graph: " + vertex + ". Consider calling io.vvasilev.natera.Graph.addVertex first to add vertex to graph");
        }
    }

    private void checkForLoop(T vertex1, T vertex2) {
        if (vertex1.equals(vertex2)) {
            throw new IllegalArgumentException("Loops are not supported: (" + vertex1 + ", " + vertex2 + ")");
        }
    }

    private void checkForEdgeDuplicate(T vertex1, T vertex2) {
        if (vertices.containsKey(vertex1) && vertices.get(vertex1).contains(vertex2)) {
            throw new IllegalArgumentException("Duplicated edge: (" + vertex1 + ", " + vertex2 + ")");
        }
    }

    /**
     *
     * @param path as list of vertices
     * @return path as list of {@link }
     */
    private List<Edge<T>> toList(List<T> path) {
        if (path == null) {
            return null;
        }
        List<Edge <T>> result = new ArrayList<>();
        for (int i=0; i < path.size() - 1; i++) {
            result.add(new SimpleEdge<>(path.get(i), path.get(i + 1)));
        }
        return result;
    }

    private List<T> findAnyPath(T vertex1, T vertex2, Set<T> visited) {
        Set<T> adjacent = vertices.getOrDefault(vertex1, Collections.emptySet());
        if (adjacent.contains(vertex2)) {
            List<T> path = new LinkedList<>();
            path.add(vertex1);
            path.add(vertex2);
            return path;
        } else {
            for (T v : adjacent) {
                if (!visited.contains(v)) {
                    visited.add(v);
                    List<T> path = findAnyPath(v, vertex2, visited);
                    if (path != null) {
                        path.add(0, vertex1);
                        return path;
                    }

                    visited.remove(v);
                }

            }
        }
        return null;
    }

    /**
     * Implementation of {@link Edge}
     *
     * @param <T>
     */
    public static class SimpleEdge<T> implements Edge<T> {

        private final T tail;
        private final T head;

        public SimpleEdge(T tail, T head) {
            this.tail = tail;
            this.head = head;
        }

        @Override
        public T getTail() {
            return tail;
        }

        @Override
        public T getHead() {
            return head;
        }
    }
}
