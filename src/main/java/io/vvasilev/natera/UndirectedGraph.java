package io.vvasilev.natera;

/**
 * Undirected implementation of {@link Graph}
 * Inherits behavior from {@link AdjacencyListGraph}
 *
 * @param <T>
 *
 * @see AdjacencyListGraph
 */
class UndirectedGraph<T> extends AdjacencyListGraph<T> implements Graph<T, Edge<T>>{

    UndirectedGraph() {
        super(true);
    }
}
