package io.vvasilev.natera;

import java.util.Arrays;
import java.util.List;

/**
 * Interface to represent an oriented link between {@link #getTail()} and {@link #getHead()}
 * stating the fact that {@link #getHead()} is reachable from {@link #getTail()}
 *
 * {@link AdjacencyListGraph} and {@link UndirectedGraph}
 * do not have explicit representation of an edge thus making the whole purpose of existence is to
 * constitute a path between two vertices in a graph.
 *
 * @param <T> type of vertices joined
 *
 * @see Graph
 */
public interface Edge<T> {

    /**
     * Returns the tail of edge
     *
     * @return vertex of user type
     */
    T getTail();

    /**
     * Returns the head of edge
     *
     * @return vertex of user type
     */
    T getHead();

    /**
     *
     * @return list where first element is tail and the last element is head
     */
    default List<T> asList() {
        return Arrays.asList(getTail(), getHead());
    }
}
