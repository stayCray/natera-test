package io.vvasilev.natera;

import java.util.List;

/**
 * Interface for a graph
 *
 *
 * The behavior is not specified if {@link Object#equals(Object)} comparisons are affected by changes in objects representing vertices.
 *
 *
 * @param <T> type of vertex
 * @param <E> type of edge
 *
 * @see AdjacencyListGraph
 * @see UndirectedGraph
 * @see DirectedGraph
 *
 */
public interface Graph<T, E extends Edge<T>> {

    /**
     * Adds vertex to the graph.
     * A vertex may exist in a graph and not belong to an edge.
     *
     * @param vertex vertex to add.
     * @throws NullPointerException if vertex being added is null
     */
    void addVertex(T vertex);

    /**
     * Adds edge to the graph.
     * Edge is specified by the pair of vertices.
     * What type of edge (unidirectional or bidirectional) will be created depends on actual implementation.
     * For directed graphs pair of vertices is ordered,
     * meaning that the first argument is the tail of the edge and the second one is the head.
     *
     * <p>Whether multiple edges (two or more edges that join the same two vertices) are allowed or not depends on actual implementation.
     *
     * <p>Whether loops (edge that joins a vertex to itself) are allowed or not depends on actual implementation.
     *
     * @param vertex1 endpoint of the edge
     * @param vertex2 endpoint of the edge
     * @throws NullPointerException if any of vertices is null
     * @throws IllegalStateException if any of vertices is not added by means of {@link #addVertex(Object)} first
     *
     */
    void addEdge(T vertex1, T vertex2);

    /**
     * Returns a list of edges between 2 vertices (path doesn’t have to be optimal)
     *
     * @param vertex1 vertex specifying the start of the path
     * @param vertex2 vertex specifying the end of the path
     * @return List of edges, null if there no path found
     * @throws NullPointerException if any of vertices is null
     * @throws IllegalStateException if any of vertices is not added by means of {@link #addVertex(Object)} first
     */
    List<E> getPath(T vertex1, T vertex2);

}
