package io.vvasilev.natera;

/**
 * Factory and utility methods for {@link Graph}
 *
 *
 */
public class Graphs {

    private Graphs() {
    }

    /**
     * Creates an empty directed simple graph.
     *
     * @param <T> type of vertices
     * @return the newly created graph
     */
    public static <T> Graph<T, ? extends Edge<T>> newDirected() {
        return new DirectedGraph<>();
    }

    /**
     * Creates an empty undirected simple graph.
     *
     * @param <T> type of vertices
     * @return the newly created graph
     */
    public static <T> Graph<T, ? extends Edge<T>> newUndirected() {
        return new UndirectedGraph<>();
    }

}
