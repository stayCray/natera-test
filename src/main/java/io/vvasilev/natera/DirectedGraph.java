package io.vvasilev.natera;

/**
 * Directed implementation of {@link Graph}
 * Inherits behavior from {@link AdjacencyListGraph}
 *
 * @param <T>
 *
 * @see AdjacencyListGraph
 */
class DirectedGraph<T> extends AdjacencyListGraph<T> implements Graph<T, Edge<T>>{

    DirectedGraph() {
        super(false);
    }
}
